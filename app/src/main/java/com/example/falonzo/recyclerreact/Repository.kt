package com.example.falonzo.recyclerreact

object Repository {

    val numericModelList: List<NumericModel> = listOf(
        NumericModel(value = 1, strValue = "1"),
        NumericModel(value = 2, strValue = "2"),
        NumericModel(value = 3, strValue = "3"),
        NumericModel(value = 4, strValue = "4"),
        NumericModel(value = 5, strValue = "5"),
        NumericModel(value = 6, strValue = "6"),
        NumericModel(value = 7, strValue = "7"),
        NumericModel(value = 8, strValue = "8"),
        NumericModel(value = 9, strValue = "9"),
        NumericModel(value = 10, strValue = "10"),
        NumericModel(value = 11, strValue = "11"),
        NumericModel(value = 12, strValue = "12"),
        NumericModel(value = 13, strValue = "13"),
        NumericModel(value = 14, strValue = "14"),
        NumericModel(value = 15, strValue = "15"),
        NumericModel(value = 16, strValue = "16")
    )

}