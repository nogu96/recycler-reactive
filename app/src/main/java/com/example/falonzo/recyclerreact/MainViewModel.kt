package com.example.falonzo.recyclerreact

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    private val mutableValue = MutableLiveData<NumericModel>()
    val displayedValue: LiveData<NumericModel>
        get() = mutableValue

    init {
    }

    fun updateValue(newValue: NumericModel) {
        mutableValue.value = newValue
    }

}