package com.example.falonzo.recyclerreact

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.falonzo.recyclerreact.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), Adapter.Listener {

    lateinit var mainViewModel: MainViewModel
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
            .apply {
                this.viewModel = mainViewModel
            }

        binding.activityMainRecyclerview.layoutManager = LinearLayoutManager(this)
        binding.activityMainRecyclerview.adapter = Adapter(this, Repository.numericModelList)

        binding.activityMainButton.setOnClickListener {
            Toast.makeText(this, mainViewModel.displayedValue.value?.strValue ?: "no value", Toast.LENGTH_SHORT).show()
        }

        mainViewModel.displayedValue.observe(this, Observer {
            binding.activityMainTextview.text = it.strValue
        })

    }

    /**Adapter.Listener*/
    override fun onItemPress(numericModel: NumericModel) {
        mainViewModel.updateValue(newValue = numericModel)
    }

}


