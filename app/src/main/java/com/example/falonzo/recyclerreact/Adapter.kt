package com.example.falonzo.recyclerreact

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class Adapter(listener: Listener, numericModelList: List<NumericModel>): RecyclerView.Adapter<ViewHolder>() {

    val numericModelList: List<NumericModel> = numericModelList
    val listener: Listener = listener

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.numeric_model_item, parent, false))

    override fun getItemCount(): Int = numericModelList.size


    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.loadData(numericModelList.get(position))
        viewHolder.itemView.setOnClickListener {
            listener.onItemPress(numericModelList.get(position))
        }
    }

    interface Listener {
        fun onItemPress(numericModel: NumericModel)
    }

}

class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    val text: TextView

    init {
        text = itemView.findViewById(R.id.numeric_model_item_text)
    }

    fun loadData(numericModel: NumericModel) {
        text.text = numericModel.value.toString()
    }

}

